$(document).ready(function() {

  $('.hero-slider').slick({
    dots: true,
    autoplay: true,
    nextArrow: '<span class="slick-next"><i class="icon-right-open-big"></i></span>',
    prevArrow: '<span class="slick-prev"><i class="icon-left-open-big"></i></span>',
  });

  $('.tiles-slider').slick({
    slidesToShow: 4,
    customPaging: 10,
    nextArrow: '<span class="slick-next-catalog"><i class="icon-right-open-big"></i></span>',
    prevArrow: '<span class="slick-prev-catalog"><i class="icon-left-open-big"></i></span>',
    responsive: [
      {
        breakpoint: 800,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 3
        }
      },
      {
        breakpoint: 650,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2
        }
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
    ]
  });
  $('.feedback-slider').slick({
    slidesToShow: 2,
    customPaging: 10,
    nextArrow: '<span class="slick-next-catalog"><i class="icon-right-open-big"></i></span>',
    prevArrow: '<span class="slick-prev-catalog"><i class="icon-left-open-big"></i></span>',
    responsive: [
      {
        breakpoint: 800,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
    ]
  });

  $('.flat-corousel-big').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    dots: false,
    arrows: false,
    fade: true,
    asNavFor: '.flat-carousel-nav'
  });
  $('.flat-carousel-nav').slick({
    slidesToShow: 3,
    slidesToScroll: 1,
    rows: 2,
    dots: false,
    asNavFor: '.flat-corousel-big',
    nextArrow: '<span class="slick-next-catalog"><i class="icon-right-open-big"></i></span>',
    prevArrow: '<span class="slick-prev-catalog"><i class="icon-left-open-big"></i></span>',
    focusOnSelect: true
  });

  var stickyElements = document.getElementsByClassName('sticky');

  for (var i = stickyElements.length - 1; i >= 0; i--) {
    Stickyfill.add(stickyElements[i]);
  }

  $(document).foundation();

  $(".tabs").on("toggled", function (event, tab) {
    $(".flat-corousel-big").slick("setPosition");
    $(".flat-carousel-nav").slick("setPosition");
  });

  var SumSlider = document.getElementById('mortgage-summ'),
      SumInput = document.getElementById('mortgage-summ-input');

  noUiSlider.create(SumSlider, {
    start: 800,
    step: 10,
    range: {
      'min': 800,
      'max': 7000
    },
    format: wNumb({
      decimals: 0
    })
  });

  SumSlider.noUiSlider.on('update', function( values, handle ) {
    SumInput.value = values[handle];
  });

  SumInput.addEventListener('change', function(){
    SumSlider.noUiSlider.set([null, this.value]);
  });

  var PercentSlider = document.getElementById('mortgage-percent'),
    PercentInput = document.getElementById('mortgage-percent-input');

  noUiSlider.create(PercentSlider, {
    start: 14,
    step: 0.1,
    range: {
      'min': 12,
      'max': 21
    },
    format: wNumb({
      decimals: 1
    })
  });

  PercentSlider.noUiSlider.on('update', function( values, handle ) {
    PercentInput.value = values[handle];
  });

  PercentInput.addEventListener('change', function(){
    PercentSlider.noUiSlider.set([null, this.value]);
  });


  $("#bookmarkme").click(function() {
    if (window.sidebar) { // Mozilla Firefox Bookmark
      window.sidebar.addPanel(location.href,document.title,"");
    } else if(window.external) { // IE Favorite
      window.external.AddFavorite(location.href,document.title); }
    else if(window.opera && window.print) { // Opera Hotlist
      this.title=document.title;
      return true;
    }
  });
});
